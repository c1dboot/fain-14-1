/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.patrick85.java;

/**
 * Die KlasseA soll mit der KlasseB in einer unidirektionalen 1-1 Beziehung stehen
 * @author Patrick Berger <patricksx@mail.de>
 */
public class KlasseA {
    
    // wir benötigen eine Objektvariable vom Typ KlasseB
    private KlasseB objB;
    
    
    // stellt eine Assoziation zu einem Objekt der KlasseB her
    public void setLink(KlasseB obj) {
        
        // falls das Objekt der KlasseA bereits assoziert ist, dann tun wir nix!
        if( this.objB != null)
            return;
        
        // wir speichern das Objekt der KlasseB
        this.objB = obj;
    }
    
    
    // Liefert das Objekt der KlasseB
    public KlasseB getLink() {
        
        return this.objB;
    }
    
    
    // Löst die Assoziation zu einem Objekt der KlasseB
    public void removeLink() {
        
        this.objB = null;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        
        return this.getClass().getSimpleName();
    }
}
