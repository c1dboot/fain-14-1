/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.patrick85.java;

/**
 *
 * @author Patrick Berger <patricksx@mail.de>
 */
public class Association {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // unsere Testobjekte
        KlasseA objA = new KlasseA();
        KlasseB objB = new KlasseB();
        
        // wir assozieren A mit B
        objA.setLink(objB);
        
        // wir geben A aus
        System.out.println(objA);
        
        // wir geben das assozierte Objekt B aus
        if (objA.getLink() != null) {
            
            System.out.println(objA.getLink() );
        }
        
        // unsere Testobjekte
        KlasseC objC = new KlasseC();
        KlasseD objD1 = new KlasseD();
        KlasseD objD2 = new KlasseD();
        KlasseD objD3 = new KlasseD();
        KlasseD objD4 = new KlasseD();
        
        objC.setLink(objD1);
        objC.setLink(objD2);
        objC.setLink(objD3);
        objC.setLink(objD4);
        
        System.out.println(objC);
        
        for(int i=0; i<4; i++) {
            
            if( objC.getLink(i) !=null) {
                
                System.out.println(objC.getLink(i));
            }
        }
    }
}
