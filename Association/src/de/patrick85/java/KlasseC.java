/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.patrick85.java;

/**
 * Die KlasseC soll mit bis zu 4 Objekten der KlasseD assoziiert werden
 * @author Patrick Berger <patricksx@mail.de>
 */
public class KlasseC {
    
    // wird gebraucht, um das Array zu "befüllen"
    private int zeiger = 0;
    
    private KlasseD[] objD = new KlasseD[4];
    
    
    public void setLink(KlasseD obj) {
        
        if( zeiger < objD.length) {
            
            this.objD[zeiger]=obj;
            zeiger++;
        }
    }
    
    public KlasseD getLink(int pos) {
        
        return this.objD[pos];
    }
    
    public void removeLink(int pos) {
        
        this.objD[pos] = null;
    }
    
    @Override
    public String toString() {
        
        return this.getClass().getSimpleName();
    }
}
